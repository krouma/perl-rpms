#
# This SPEC file was automatically generated using the cpantorpm
# script.
#
#    Package:           perl-Perl-LanguageServer
#    Version:           v2.3.0
#    cpantorpm version: 1.11
#    Date:              Wed Feb 23 2022
#    Command:
# /usr/local/bin/cpantorpm --vers v2.3.0 --spec-only Perl\:\:LanguageServer
#

Name:           perl-Perl-LanguageServer
Version:        2.3.0
Release:        1%{?dist}
Summary:        Language Server and Debug Protocol Adapter for Perl
License:        Artistic 2.0
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Perl-LanguageServer/
BugURL:         http://search.cpan.org/dist/Perl-LanguageServer/
BuildArch:      noarch
Source0:        https://cpan.metacpan.org/authors/id/G/GR/GRICHTER/Perl-LanguageServer-%{version}.tar.gz

#
# Unfortunately, the automatic provides and requires do NOT always work (it
# was broken on the very first platform I worked on).  We'll get the list
# of provides and requires manually (using the RPM tools if they work, or
# by parsing the files otherwise) and manually specify them in this SPEC file.
#

AutoReqProv:    no
AutoReq:        no
AutoProv:       no

Provides:       perl(DB) = 1.07
Provides:       perl(Perl::LanguageServer) = 2.3.0
Provides:       perl(Perl::LanguageServer::DebuggerInterface) = v2.3.0
Provides:       perl(Perl::LanguageServer::DebuggerProcess) = v2.3.0
Provides:       perl(Perl::LanguageServer::DevTool) = v2.3.0
Provides:       perl(Perl::LanguageServer::IO) = v2.3.0
Provides:       perl(Perl::LanguageServer::Methods) = v2.3.0
Provides:       perl(Perl::LanguageServer::Methods::DebugAdapter) = v2.3.0
Provides:       perl(Perl::LanguageServer::Methods::DebugAdapterInterface) = v2.3.0
Provides:       perl(Perl::LanguageServer::Methods::textDocument) = v2.3.0
Provides:       perl(Perl::LanguageServer::Methods::workspace) = v2.3.0
Provides:       perl(Perl::LanguageServer::Parser) = v2.3.0
Provides:       perl(Perl::LanguageServer::Req) = v2.3.0
Provides:       perl(Perl::LanguageServer::SyntaxChecker) = v2.3.0
Provides:       perl(Perl::LanguageServer::Workspace) = v2.3.0
Requires:       perl >= 5.014
Requires:       perl(Compiler::Lexer) >= 0.23
Requires:       perl(AnyEvent)
Requires:       perl(AnyEvent::AIO)
Requires:       perl(Class::Refresh)
Requires:       perl(Coro)
Requires:       perl(Data::Dump)
Requires:       perl(IO::AIO)
Requires:       perl(JSON)
Requires:       perl(Moose)
Requires:       perl(PadWalker)
Requires:       perl(Scalar::Util)
BuildRequires:  perl >= 5.014
BuildRequires:  perl(Compiler::Lexer) >= 0.23
BuildRequires:  perl(AnyEvent)
BuildRequires:  perl(AnyEvent::AIO)
BuildRequires:  perl(Class::Refresh)
BuildRequires:  perl(Coro)
BuildRequires:  perl(Data::Dump)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(IO::AIO)
BuildRequires:  perl(JSON)
BuildRequires:  perl(Moose)
BuildRequires:  perl(PadWalker)
BuildRequires:  perl(Scalar::Util)
BuildRequires:  perl(Test::More)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
This is a Language Server and Debug Protocol Adapter for Perl

It implements the Language Server Protocol which provides
syntax-checking, symbol search, etc. Perl to various editors, for
example Visual Studio Code or Atom.


%prep

rm -rf %{_builddir}/Perl-LanguageServer-%{version}
%setup -D -n Perl-LanguageServer-v2.3.0
chmod -R u+w %{_builddir}/Perl-LanguageServer-%{version}

if [ -f pm_to_blib ]; then rm -f pm_to_blib; fi

%build

%{__perl} Makefile.PL OPTIMIZE="$RPM_OPT_FLAGS" INSTALLDIRS=site INSTALLSITEBIN=%{_bindir} INSTALLSITESCRIPT=%{_bindir} INSTALLSITEMAN1DIR=%{_mandir}/man1 INSTALLSITEMAN3DIR=%{_mandir}/man3 INSTALLSCRIPT=%{_bindir}
make %{?_smp_mflags}

#
# This is included here instead of in the 'check' section because
# older versions of rpmbuild (such as the one distributed with RHEL5)
# do not do 'check' by default.
#

if [ -z "$RPMBUILD_NOTESTS" ]; then
   make test
fi

%install

rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
%{_fixperms} $RPM_BUILD_ROOT/*

%clean

rm -rf $RPM_BUILD_ROOT

%files

%defattr(-,root,root,-)
%{perl_sitelib}/*
%{_mandir}/man3/*

%changelog
* Wed Feb 23 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> v2.3.0-1
- Generated using cpantorpm

